/*

This file was used to test the SQL query that is used by ../zync-db to
determine which steps need to be taken in order to recursively synchronise two
ZFS filesystems.

If you 'diff' this file with zync-db, the query here and the one in the zync-db
script should be identical, with the exception of the 'param' sub-query.

Requirements:
    sqlite3 version 3.39.0 or later (2022-06-25)

This sql script uses an in-memory database, so no setup is required.

To run:

    % sqlite3 < test.sql

Expected Output:

+-------------------------------+--------------+---------------------------------+----------------------------------------+------------+------------+
|            send_fs            |    action    |            send_args            |               recv_args                | snap_count | size_bytes |
+-------------------------------+--------------+---------------------------------+----------------------------------------+------------+------------+
| primary/nas1                  | none         | ~                               | ~                                      | ~          | ~          |
| primary/nas1/chaos            | update       | @magic|primary/nas1/chaos@oops  | secondary/nas1/backup/chaos            | 3          | 56         |
| primary/nas1/deleted          | destroy_fs   | ~                               | secondary/nas1/backup/deleted          | ~          | ~          |
| primary/nas1/empty            | none         | ~                               | ~                                      | ~          | ~          |
| primary/nas1/excluded         | exclude      | ~                               | ~                                      | ~          | ~          |
| primary/nas1/interrupted      | resume       | tok-2                           | secondary/nas1/backup/interrupted      | 1          | ~          |
| primary/nas1/new              | create       | ~                               | secondary/nas1/backup/new              | ~          | ~          |
| primary/nas1/new              | initial      | primary/nas1/new@a              | secondary/nas1/backup/new              | 1          | 1          |
| primary/nas1/new              | update       | @a|primary/nas1/new@b           | secondary/nas1/backup/new              | 1          | 2          |
| primary/nas1/new/deep         | create       | ~                               | secondary/nas1/backup/new/deep         | ~          | ~          |
| primary/nas1/new/deep         | initial      | primary/nas1/new/deep@a         | secondary/nas1/backup/new/deep         | 1          | 1          |
| primary/nas1/new/deep         | update       | @a|primary/nas1/new/deep@c      | secondary/nas1/backup/new/deep         | 2          | 6          |
| primary/nas1/new/empty        | none         | ~                               | ~                                      | ~          | ~          |
| primary/nas1/new/empty/deeper | create       | ~                               | secondary/nas1/backup/new/empty/deeper | ~          | ~          |
| primary/nas1/new/empty/deeper | initial      | primary/nas1/new/empty/deeper@a | secondary/nas1/backup/new/empty/deeper | 1          | 1          |
| primary/nas1/outdated         | resume       | tok-1                           | secondary/nas1/backup/outdated         | 1          | ~          |
| primary/nas1/outdated         | destroy_snap | ~                               | secondary/nas1/backup/outdated@a       | 1          | ~          |
| primary/nas1/outdated         | destroy_snap | ~                               | secondary/nas1/backup/outdated@b       | 1          | ~          |
| primary/nas1/outdated         | initial      | primary/nas1/outdated@c         | secondary/nas1/backup/outdated         | 1          | 4          |
| primary/nas1/outdated         | update       | @c|primary/nas1/outdated@d      | secondary/nas1/backup/outdated         | 1          | 8          |
| primary/nas1/synced           | none         | ~                               | ~                                      | ~          | ~          |
| primary/nas1/typical          | destroy_snap | ~                               | secondary/nas1/backup/typical@a        | 1          | ~          |
| primary/nas1/typical          | update       | @c|primary/nas1/typical@d       | secondary/nas1/backup/typical          | 1          | 8          |
+-------------------------------+--------------+---------------------------------+----------------------------------------+------------+------------+

Or, split into individual use cases:

- `primary/nas1`

  The top-level filesystem, 'primary/nas' has no snapshots in this example, so
  there is nothing to be done. If a sub-filesystem needs to be created, it will
  be created with 'zfs create -p' which automatically creates any missing
  intermediary filesystems.

  All other filesystems are direct descendents of the `primary/nas1` filesystem
  tree on the sender.

- `chaos`

  The 'chaos' filesystem demonstrates the use of "chaotic" snapshot names.
  Snapshots are always processed in chronological order, so that dependencies
  between snapshots are not interrupted.

- `deleted`

  The 'deleted' filesystem demostrated what happens when a file system is deleted
  on the sender but not the receiver.

  Currently, zync does NOT destroy filesystems on the receiver, as this would
  completely defeat the purpose of "keeping a backup".

  Instead, missing filesystems can be reported (via the -v / verbose option)
  and handled manually.

- `empty`

  The 'empty' filesystem demonstrates that a filesystem which never had any
  snapshots will be processed, but that no actions are performed.

- `excluded`

  The 'excluded' filesystem has snapshots, but has been explicitly marked for
  non-transfer by setting its `zync:exclude` ZFS property to "on".

  I don't expect this to be used often, and it not very flexible, but if, for
  example, you have one file system which is too large to be replicated, you
  can selectively exclude it, while still replicating all other filesystems.

- `interrupted`

  The 'interrupted' filesystem emulates a transfer which was previously interrupted,
  and then there are no common snapshots between the sender and receiver, so an
  initial snapshot copy is required.

  Note: resuming interrupted transfers leaves us in an unknown state! Resume
  tokens allow us to recover from an unexpected interruption, but they provide no
  information about the snapshot(s) that was(were) being transerred.

  NOT YET IMPLEMENTED: resumption of previous interrupted transfers _should_ be
  handled _first_ and once finished, the current state of at least the receiver
  should be re-collected and this query should be run again on the new data.

- `new`

  The 'new' filesystem show how a new filesystem on the sender should be processed.

  The 3 steps required on the receiver, in order, are:
    1. create a new, empty filesystem
    2. perform a full copy of the oldest snapshot on the sender
    3. perform an incremental copy of all snapshots between the oldest and newest

- `new/deep`

  The 'new/deep' filesystem demonstrates that children of a new filesystem
  (see 'new' above) need to be processed _after_ their parent filesystem, and
  follow exactly the same 3-step creation procedure.

- `new/empty`

  The 'new/empty' filesystem again demonstrates that empty child-filesystems of a
  new parent filesystem are handled properly, i.e. they should be ignored :-)

- `new/empty/deeper`

  The 'new/empty/deeper' filesystem however, shows that empty filesystems will
  still be implicitly create if they themselves contain children with snapshots.

- `outdated`

  The 'outdated' filesystem demonstrates what happens when all of the
  receiver's snapshots are older than the oldest snapshot still available on
  the sending filesystem.

  In this case, there is no common state between the two filesystems, so we
  need to delete all of the snapshots on the receiver and start transferring
  snapshots from the sender as if it were a new filesystem.

  To make things "more interesting", we also have a resume token.

  i.e.:
    - first resume the interrupted transfer
    - here, we assume that we still have no common snapshots
      (see the comments for the `interrupted` filesystem, above)
    - destroy missing snapshots
    - perform an initial snapshot copy
    - incrementally copy all remaining snapshots

- `synced`

  The 'synced' filesystem demonstrates that a large filesystem, which has
  already been completely replicated, does not need any additional actions to
  be taken.

- `typical`

  Finally, the 'typical' filessystem demonstrates a typical update of a system
  which was previously replicated, and has since changed.

  Only the changes need to be updated, i.e.: missing snapshots deleted and new
  snapshots are incrementally copied over the most recent common snapshot.

*/

.timer on

create table fs_info(
    host                 text not null,
    fs                   text not null,
    receive_resume_token text,
    zync_exclude         text,
    --
    primary key ( host, fs )
);

insert into fs_info( host, fs, receive_resume_token, zync_exclude )
values

    ( 'nas1', 'primary',                           null,    null ), -- top-level primary filesystem
    ( 'nas1', 'primary/nas1',                      null,    null ), -- primary storage for nas1 (e.g. media server)
    ( 'nas1', 'primary/nas1/chaos',                null,    null ), -- fs with strangely names snapshots
--  ( 'nas1', 'primary/nas1/deleted',              null,    null ), -- entire fs deleted from sender
    ( 'nas1', 'primary/nas1/empty',                null,    null ), -- empty fs should not be copied
    ( 'nas1', 'primary/nas1/excluded',             null,    "on" ), -- a fs which should never be synced
    ( 'nas1', 'primary/nas1/interrupted',          null,    null ), -- a fs which failed to perform an initial snapshot copy
    ( 'nas1', 'primary/nas1/new',                  null,    null ), -- new fs tree to be created
    ( 'nas1', 'primary/nas1/new/deep',             null,    null ), -- new fs to be created
    ( 'nas1', 'primary/nas1/new/empty',            null,    null ), -- new fs without snapshots, should not be copied
    ( 'nas1', 'primary/nas1/new/empty/deeper',     null,    null ), -- a new child of a parent without snapshots
    ( 'nas1', 'primary/nas1/outdated',             null,    null ), -- fs with no overlapping snapshots - receiver needs to be cleaned before new initial copy
    ( 'nas1', 'primary/nas1/synced',               null,    null ), -- a fs which has already been synced
    ( 'nas1', 'primary/nas1/typical',              null,    null ), -- a typical fs to be synced
    --
    ( 'nas1', 'secondary',                         null,    null ),
    ( 'nas1', 'secondary/nas1',                    null,    null ),
    ( 'nas1', 'secondary/nas1/backup',             null,    null ),
    ( 'nas1', 'secondary/nas1/backup/chaos',       null,    null ),
    ( 'nas1', 'secondary/nas1/backup/deleted',     null,    null ),
    ( 'nas1', 'secondary/nas1/backup/outdated',    'tok-1', null ),
    ( 'nas1', 'secondary/nas1/backup/interrupted', 'tok-2', null ),
    ( 'nas1', 'secondary/nas1/backup/synced',      null,    null ),
    ( 'nas1', 'secondary/nas1/backup/typical',     null,    null ),
    --
    ( 'nas2', 'tertiary',                          null,    null )

;

create table snap_info(
    host     text    not null,
    fs       text    not null,
    snap     text    not null,
    guid     text    not null,
    creation integer,
    written  integer,
    --
    primary key ( host, fs, snap, guid )
);

-- timestamps replaced with simple integers to simplify testing,
-- only used to ensure the correct order
insert into snap_info
    ( host,   fs,                        snap,     guid,      creation, written )
values

-- sender
--  ( 'nas1', 'primary/nas1/typical',           '@a',     'n1f@a1',  1, 1       ),   -- deleted
    ( 'nas1', 'primary/nas1/typical',           '@b',     'n1f@b1',   2, 2       ),
    ( 'nas1', 'primary/nas1/typical',           '@c',     'n1f@c1',   3, 4       ),
    ( 'nas1', 'primary/nas1/typical',           '@d',     'n1f@d1',   4, 8       ),
    --
    ( 'nas1', 'primary/nas1/synced',            '@a',     'n1b@a1',   1, 1       ),
    ( 'nas1', 'primary/nas1/synced',            '@b',     'n1b@b1',   2, 2       ),
    --
--  ( 'nas1', 'primary/nas1/outdated',          '@a',     'n1z@a1',  1, 1       ),   -- deleted
--  ( 'nas1', 'primary/nas1/outdated',          '@b',     'n1z@b1',  2, 2       ),   -- deleted
    ( 'nas1', 'primary/nas1/outdated',          '@c',     'n1z@c1',   3, 4       ),
    ( 'nas1', 'primary/nas1/outdated',          '@d',     'n1z@d1',   4, 8       ),
    --
    ( 'nas1', 'primary/nas1/chaos',             '@zap',   'n1c@z1',   1, 1       ),
    ( 'nas1', 'primary/nas1/chaos',             '@cool',  'n1c@c1',   2, 2       ),
    ( 'nas1', 'primary/nas1/chaos',             '@magic', 'n1c@m1',   3, 4       ),
    ( 'nas1', 'primary/nas1/chaos',             '@bang',  'n1c@b1',   4, 8       ),
    ( 'nas1', 'primary/nas1/chaos',             '@pop',   'n1c@p1',   5, 16      ),
    ( 'nas1', 'primary/nas1/chaos',             '@oops',  'n1c@o1',   6, 32      ),
    --
    ( 'nas1', 'primary/nas1/new',               '@a',     'n1n@a1',   1, 1       ),
    ( 'nas1', 'primary/nas1/new',               '@b',     'n1n@b1',   2, 2       ),
    --
    ( 'nas1', 'primary/nas1/new/deep',          '@a',     'n1nd@a1',  1, 1       ),
    ( 'nas1', 'primary/nas1/new/deep',          '@b',     'n1nd@b1',  2, 2       ),
    ( 'nas1', 'primary/nas1/new/deep',          '@c',     'n1nd@c1',  3, 4       ),
    --
    ( 'nas1', 'primary/nas1/new/empty/deeper',  '@a',     'n1ned@a1', 1, 1       ),
    --
    ( 'nas1', 'primary/nas1/excluded',          '@a',     'n1e@a1',   1, 99      ), -- snapshots which should never be synced
    ( 'nas1', 'primary/nas1/excluded',          '@b',     'n1e@b1',   2, 99      ),
-- receiver
    -- previously synced snapshots
    ( 'nas1', 'secondary/nas1/backup/chaos',    '@zap',   'n1c@z1',   1, 1       ),
    ( 'nas1', 'secondary/nas1/backup/chaos',    '@cool',  'n1c@c1',   2, 2       ),
    ( 'nas1', 'secondary/nas1/backup/chaos',    '@magic', 'n1c@m1',   3, 4       ),
    --
    ( 'nas1', 'secondary/nas1/backup/deleted',  '@a',     'n1d@a1',   1, 1       ),
    ( 'nas1', 'secondary/nas1/backup/deleted',  '@b',     'n1d@b1',   2, 2       ),
    ( 'nas1', 'secondary/nas1/backup/deleted',  '@c',     'n1d@c1',   3, 4       ),
    --
    ( 'nas1', 'secondary/nas1/backup/outdated', '@a',     'n1z@a1',   1, 1       ),
    ( 'nas1', 'secondary/nas1/backup/outdated', '@b',     'n1z@b1',   2, 2       ),
    --
    ( 'nas1', 'secondary/nas1/backup/synced',   '@a',     'n1b@a1',   1, 1       ),
    ( 'nas1', 'secondary/nas1/backup/synced',   '@b',     'n1b@b1',   2, 2       ),
    --
    ( 'nas1', 'secondary/nas1/backup/typical',  '@a',     'n1f@a1',   1, 1       ),
    ( 'nas1', 'secondary/nas1/backup/typical',  '@c',     'n1f@c1',   3, 4       )

;

--
-- Known Issues:
--  snapshots are identified by filesystem, namd, creation time and guid
--    filesystem@snapshot
--      - is used by all zfs commands
--      - uniquely identifies a "currently existing snapshot"
--    filesystem + guid
--      - uniquely identifies a snapshot accross systems and time!
--      - if a snapshot is deleted and a new snapshot is created with the same
--        name, the new one will have a different guid!
--    filesystem + snapshot creation time
--      - used to infer dependencies between snapshots
--      - it IS possible to create multiple snapshots in the same filesystem
--        with the same timestamp!
--        (snapshots don't take long to create and there are 1000 ms in every
--        creation timestamp)
--
--  In this query, snapshots are ordered by their timestamps, but uniquely
--  matched by guid. Which snapshots were created between the first/last
--  snapshot to be transferred can therefor be a bit... vague :-(
--
--  Hopefully, this only effects the calculation of the total size of the data
--  to be transferred.
--


.param init
.param set :send_host nas1
.param set :send_fs   primary/nas1
.param set :recv_host nas1
.param set :recv_fs   secondary/nas1/backup
-- .param set :recv_host nas2
-- .param set :recv_fs   tertiary/nas1

with

param as (
    select
        :send_host  as send_host,
        :send_fs    as send_fs,
        :recv_host  as recv_host,
        :recv_fs    as recv_fs
),

send_fs as (
    -- Collect information about the sending filesystem(s)
    --  - 1 row per filesystem
    --  - primary key is the sub_fs
    --    - i.e. the filesystem path relative to the requested sender
    --      filesystem (send_fs) on the sending host (send_host)
    --  - assumption that filesystem names are hierarchical
    --    - their common prefix is enough to get all sub-filesystems
    --    - sorting by name will cause parent filesystems to appear before child filesystems
    select distinct
        trim( substr( fsi.fs, length( p.send_fs ) + 2 ), '/' ) as sub_fs,      -- fs join pattern
        fsi.fs                                                 as fs,          -- full fs path
        fsi.zync_exclude                                       as zync_exclude -- "on": don't send anything for this fs
    from
             param   p
        join fs_info fsi  on  fsi.host = p.send_host
                          and trim( substr( fsi.fs, 1, length( p.send_fs ) + 1 ), '/' ) = p.send_fs
),

recv_fs as (
    -- Collect information about the receiving filesystem(s)
    --  - 1 row per filesystem
    select distinct
        trim( substr( fsi.fs, length( p.recv_fs ) + 2 ), '/' ) as sub_fs, -- fs join pattern
        fsi.fs                                                 as fs,     -- full fs path
        fsi.receive_resume_token                               as token   -- token used to recommence an interrupted transfer
    from
             param   p
        join fs_info fsi  on  fsi.host = p.recv_host
                          and trim( substr( fsi.fs, 1, length( p.recv_fs ) + 1 ), '/' ) = p.recv_fs
),

all_fs as (
    -- Unified set of (virtual) filesystem names for both sender and receiver.
    --  - 1 row per sub_fs
    --  - all information required for following queries
    --    - no need to re-join with fs_info
    --  - sender & receiver need slightly different properties
    select
        coalesce( sfs.sub_fs, rfs.sub_fs )                              as sub_fs,
        trim( coalesce( sfs.fs, p.send_fs || '/' || rfs.sub_fs ), '/' ) as send_fs,
        iif( sfs.fs is null, 'missing', 'exists' )                      as send_exists,
        trim( coalesce( rfs.fs, p.recv_fs || '/' || sfs.sub_fs ), '/' ) as recv_fs,
        iif( rfs.fs is null, 'missing', 'exists' )                      as recv_exists
    from
                  send_fs sfs
        full join recv_fs rfs on rfs.sub_fs = sfs.sub_fs
             join param   p   on 1 = 1   -- must go after 'full join'
),

send_snap as (
    -- Summary of all snapshots in the sender filesystem(s)
    --  - 1 row per snapshot
    --    - 0-n rows per filesystem
    select
        sfs.sub_fs   as sub_fs,    -- fs join pattern
        sfs.fs       as fs,        -- snap location, required for zfs commands
        ssi.snap     as snap,      -- snap name, required for zfs commands
        ssi.guid     as guid,      -- unique snap id
        ssi.creation as creation,  -- required for ordering
        ssi.written  as size_bytes -- amount of data required to transfer this snapshot
    from
             send_fs   sfs
        join snap_info ssi on ssi.fs = sfs.fs
    where
        coalesce( sfs.zync_exclude, 'off' ) != 'on'  -- suppress all snapshots from excluded filesystems
),

recv_snap as (
    -- Summary of snapshots on corresponding receiver filesystems
    --  - 1 row per snapshot
    --    - 0..n rows per filesystem
    select
        rfs.sub_fs   as sub_fs,  -- fs join pattern
        rsi.fs       as fs,      -- snap location, required for zfs commands
        rsi.snap     as snap,    -- snap name, required for zfs commands
        rsi.guid     as guid,    -- unique snap id
        rsi.creation as creation -- required for ordering
    from
             recv_fs   rfs
        join snap_info rsi on rsi.fs = rfs.fs
),

send_first_last as (
    -- Determine the oldest and newest snapshot on the sender
    --  - 0..1 row per filesystem
    --  - snapshots are ordered by creation time
    --    - but uniquely identified by guid
    --  - only need the size of the first snapshot here,
    --    for a possible initial copy
    --    see below for the other snapshot sizes
    select distinct
        ss.sub_fs,
        -- first snap
        first_value( ss.snap       ) over fs_snaps as first_snap,
        first_value( ss.guid       ) over fs_snaps as first_guid,
        first_value( ss.creation   ) over fs_snaps as first_creation,
        first_value( ss.size_bytes ) over fs_snaps as first_size_bytes,
        -- last snap
        last_value(  ss.snap       ) over fs_snaps as last_snap,
        last_value(  ss.guid       ) over fs_snaps as last_guid,
        last_value(  ss.creation   ) over fs_snaps as last_creation
    from
        send_snap ss
    window
        fs_snaps as (
                    partition by ss.sub_fs
                    order by     ss.creation
                    rows between unbounded preceding
                             and unbounded following
                    )
),

send_common as (
    -- Find the most recent, common snapshot between the sender and receiver
    --  - 0..1 row per filesystem
    --  - use sqlite specialty:
    --    - max() causes all fields to be taken from the same row as the row
    --      which contains the max value
    --      - see https://sqlite.org/quirks.html#aggregate_queries_can_contain_non_aggregate_result_columns_that_are_not_in_the_group_by_clause
    select
        ss.sub_fs          as sub_fs,
        ss.snap            as common_snap,
        ss.guid            as common_guid,
        max( ss.creation ) as common_creation
    from
             send_snap ss
        join recv_snap rs   on  rs.sub_fs = ss.sub_fs
                            and rs.guid   = ss.guid
    group by
        ss.sub_fs
),

send_overview as (
    -- Finally, this full join determines all of the similarities and
    -- differences between the sending and receiving filesystem(s)
    --  - 1 .. n rows per sending filesystem
    --  - 1 row per snapshot
    --    - or a null row for filesystems without snapshots
    --  - all details required for later joins
    select
        sfs.sub_fs           as sub_fs,
        sfl.first_snap       as first_snap,
        sfl.first_guid       as first_guid,
        sfl.first_creation   as first_creation,
        sfl.first_size_bytes as first_size_bytes,
        sc.common_snap       as common_snap,
        sc.common_guid       as common_guid,
        sc.common_creation   as common_creation,
        sfl.last_snap        as last_snap,
        sfl.last_guid        as last_guid,
        sfl.last_creation    as last_creation
    from
                  send_fs         sfs
        left join send_first_last sfl on sfl.sub_fs = sfs.sub_fs
        left join send_common     sc  on sc.sub_fs  = sfs.sub_fs
        left join send_snap       ss  on ss.sub_fs  = sfs.sub_fs
    group by
        sfs.sub_fs
),

--
-- Actions
--
-- All of the following queries MUST return the same structure.
-- This is the structure that is returned to the shell for processing.
--
--  - 1 row per action
--    - 'none' is also an action ;-)
--    - at least 1 row per filesystem on the sender or receiver!
--    - e.g. 'purge' for filesystems which have been deleted on the sender, but
--      which still exist on the receiver
--  - hostnames are NOT returned, as these are always pre-determined
--

recv_resume as (
    -- Recommence a transaction which was interrupted.
    -- ZFS records which blocks have been successfully transferred to the
    -- receiver. If a transfer is cancelled (e.g. network failure, impatient
    -- admin, ...) then it can be restarted without having to re-transmit all
    -- of the previously transferred blocks.
    -- Thus, if a zync synchonisation is interrupted after transferring 99% of
    -- the data, the next run only needs to complete the missing 1%
    --
    -- Sadly, there seems to be no way to determine WHICH snapshot was being
    -- transferred, or the remaining amount of data to be transferred.
    select
        rfs.sub_fs     as sub_fs,
        'resume'       as action,
        rfs.token      as send_args,
        rfs.fs         as recv_args,
        1              as snap_count,
        null           as size_bytes  -- unknown size
    from
        recv_fs rfs
    where
        rfs.token is not null
),

send_exclude as (
    -- Identify sender filesystems which were explicitly excluded from synchronisation
    select
        sfs.sub_fs as sub_fs,
        'exclude'  as action,
        null       as send_args,
        null       as recv_args,
        null       as snap_count,
        null       as size_bytes
    from
        send_fs sfs
    where
        coalesce( sfs.zync_exclude, 'off' ) = 'on'
),

recv_destroy_fs as (
    -- Identify filesystems on the receiver which no longer exist on the
    -- sender.
    --
    -- IMPORTANT: filesystems should NOT be automatically destroyed!
    -- doing so immediately destroys any chance of recovery!
    --
    -- My strategy is to mark these filesystems as being 'archived'
    -- (e.g. via a custom zfs property).
    -- Only when a filesystem has been archived for a "long time" do I clean
    -- these up manually.
    --
    -- TODO: come up with a less manual way to handle filesystems which have
    -- been deleted on the sender.
    select
        afs.sub_fs   as sub_fs,
        'destroy_fs' as action,
        null         as send_args,
        afs.recv_fs  as recv_args,
        null         as snap_count,
        null         as size_bytes
    from
        all_fs afs
    where
        afs.send_exists = 'missing'
),

recv_destroy_snap as (
    -- Determine which snapshots can be destroyed from the receiver.
    --
    -- Expired snapshots should be destroyed if the reciever is intended to be
    -- a mirror of the sender.
    --
    -- Expired snapshots should NOT be destroyed if the receiver is intended as
    -- an archive.
    --
    -- I personally DO destroy old snapshots, BECAUSE I keep up to 6 months
    -- history in form of multiple, periodic snapshots (e.g., daily, weekly and
    -- monthly snapshots), therefore, snapshots are only destroyed when there
    -- are new snapshots to take their place.
    select
        so.sub_fs        as sub_fs,
        'destroy_snap'   as action,
        null             as send_args,
        rs.fs || rs.snap as recv_args,
        1                as snap_count,
        null             as size_bytes
    from
                  send_overview so
             join recv_snap     rs  on  rs.sub_fs   = so.sub_fs
                                    and rs.creation < coalesce( so.common_creation, so.first_creation )
        left join send_snap     ss  on  ss.sub_fs   = so.sub_fs
                                    and ss.guid     = rs.guid
    where
        ss.guid is null
),

recv_create as (
    -- Identify filesystems which exist on the sender, but not (yet) on the
    -- receiver.
    select
        so.sub_fs   as sub_fs,
        'create'    as action,
        null        as send_args,
        afs.recv_fs as recv_args,
        null        as snap_count,
        null        as size_bytes
    from
             send_overview so
        join all_fs        afs  on  afs.sub_fs      = so.sub_fs
                                and afs.recv_exists = 'missing'
    where
        so.last_guid is not null
),

send_initial as (
    -- Identify filesystems which require an initial snapshot to be
    -- transferred, i.e. when no common snapshot can be found on both sender
    -- and receiver.
    select
        so.sub_fs           as sub_fs,
        'initial'           as action,
        ss.fs || ss.snap    as send_args,
        afs.recv_fs         as recv_args,
        1                   as snap_count,
        so.first_size_bytes as size_bytes
    from
             send_overview so
        join send_snap     ss   on  ss.sub_fs  = so.sub_fs
                                and ss.guid    = so.first_guid
        join all_fs        afs  on  afs.sub_fs = so.sub_fs
    where
        so.common_guid is null
),

send_update_size as (
    -- Determine the total size, in bytes, to be transferred incrementally.
    -- Incremental updates can transfer any number of snapshots, so we need to
    -- accumulate the sizes of all snapshots between the most recent common
    -- snapshot and the most latest snapshot on the sender.
    select
        so.sub_fs              as sub_fs,
        count( ss.size_bytes ) as snap_count,
        sum(   ss.size_bytes ) as size_bytes
    from
             send_overview so
        join send_snap     ss on  ss.sub_fs    = so.sub_fs
                              and ss.creation  > coalesce( so.common_creation, so.first_creation )
                              and ss.creation <= so.last_creation
    group by
        so.sub_fs
),

send_updates as (
    -- Group all snapshots which can be transferred together in a single
    -- from .. till pair.
    select
        so.sub_fs             as sub_fs,
        'update'              as action,
        ss1.snap
        || '|'  -- non-whitespace character to avoid parsing problems in sh
        || ssn.fs || ssn.snap as send_args,
        afs.recv_fs           as recv_args,
        sus.snap_count        as snap_count,
        sus.size_bytes        as size_bytes
    from
             send_overview    so
        join send_snap        ss1 on  ss1.sub_fs = so.sub_fs
                                  and ss1.guid   = coalesce( so.common_guid, so.first_guid )
        join send_snap        ssn on  ssn.sub_fs = so.sub_fs
                                  and ssn.guid   = so.last_guid
        join all_fs           afs on  afs.sub_fs = so.sub_fs
        join send_update_size sus on sus.sub_fs  = so.sub_fs
    where
        coalesce( so.common_guid, so.first_guid ) != so.last_guid
),

all_actions as (
    -- Prioritise all actions
              select 1 as prio, sub_fs, action, send_args, recv_args, snap_count, size_bytes from send_exclude
    union all select 2 as prio, sub_fs, action, send_args, recv_args, snap_count, size_bytes from recv_resume
    union all select 3 as prio, sub_fs, action, send_args, recv_args, snap_count, size_bytes from recv_destroy_fs
    union all select 4 as prio, sub_fs, action, send_args, recv_args, snap_count, size_bytes from recv_destroy_snap
    union all select 5 as prio, sub_fs, action, send_args, recv_args, snap_count, size_bytes from recv_create
    union all select 6 as prio, sub_fs, action, send_args, recv_args, snap_count, size_bytes from send_initial
    union all select 7 as prio, sub_fs, action, send_args, recv_args, snap_count, size_bytes from send_updates
),

prio_actions as (
    select
        afs.send_fs                   as send_fs,
        coalesce( aa.action, 'none' ) as action,
        aa.send_args                  as send_args,
        aa.recv_args                  as recv_args,
        aa.snap_count                 as snap_count,
        aa.size_bytes                 as size_bytes
    from
                  all_fs      afs
        left join all_actions aa  on aa.sub_fs = afs.sub_fs
    order by
        afs.send_fs,
        aa.prio
)

-- Test Queries:
-- select * from param;
-- select * from send_fs;
-- select * from recv_fs;
-- select * from all_fs;
-- select * from send_first_last;
-- select * from send_common;
-- select * from send_overview;
-- select * from send_exclude;
-- select * from recv_resume;
-- select * from recv_destroy_fs;
-- select * from recv_destroy_snap;
-- select * from recv_create;
-- select * from send_initial;
-- select * from send_updates;
select * from prio_actions;
