# Zync - ZFS Snapshot Synchroniser

Version: v0.0.3

This project implements what a naive user might expect "zfs send -R | zfs recv"
to do, i.e. recursively replicate a tree of ZFS filesystems, but without the
caveats and problems that "zfs send -R" doesn't handle.
(see [Why not "just use `zfs send -R`" ?](#why-not-just-use-zfs-send-r) below)

As for "Why use ZFS snapshots for backups?", well, the simple reasons would be
  - consistency: ZFS uses block hashes to ensure that no data is corrupted.
  - speed: anecdotally, I backup 4TB daily, in about 10 seconds.
  - history: by rotating a set of snapshots it is  possible to go back-in-time
    to recover those files you accidentally deleted.

This implementation is intentionally spartanic - it is just a single sh script
with no dependencies beyond typical, standard unix system tools.

If you're looking for a polished, well tested and supported(?!) system to
automate your backup process, you might like to try one of the systems listed
at the bottom of this file.

## New 2024-02-23

`zync-db` is a variant which uses an SQLite database to determine what needs
to be done.

Additional Features:
  - faster
  - better 'dry run' handling
  - reports the number and size of snapshot transfers
  - history tracking

However...
  - it also has some bugs which I have not taken the time to solve
  - top-level trees seem to confuse it
  - the db takes a point-in-time image of the source and target filesystems,
    which could lead to inconsistencies

## Features

Recursive replication of all snapshots of multiple filesystems to (possibly
remote) backup filesystems.

The ability to add or remove filesystems on the source

Auto-recovery after:

- A transfer has been interrupted

  e.g. dropped network connection, full disk or system crash

- Some file systems were synchronised, while others weren't

  e.g. syncronise your live work filesystems every hour,
  but only update your DVD collection once a week

However:

- Deleted filesystems are NOT removed from the target system (_yet_)

- Renamed filesystems are DUPLICATED on the target system,

  i.e.: a complete copy of the renamed filesystem (and its children!)
  will be available on the target system under both the old and
  new names. However, Only the new name will receive updates.

- Encrypted filesystems are not supported (yet).

  As of early 2024, there seems to be an issue with zfs native encryption and
  the sending/receiving of snapshots.

  Besides that, zfs enforces different requirements for the transfer of
  encrypted snapshots, and I have no yet worked out how to make that work
  reliably, in all scenarios.

- NOT YET IMPLEMENTED (as of 2024-02-22)
  resumption of previous interrupted transfers _should_ be handled _first_
  (assumption: a previous transfer could only be started if the receiving
  filesystem existed) and once finished, the current state of at least the
  receiver should be re-collected and this query should be run again on the new
  data.

No additional requirements beyond "Unix" and "ZFS" (*)

(*) only tested on FreeBSD 14.0 as of 2024-02-15

## Installation

Just copy the `zync` script from this directory onto your ZFS server.

It has been deliberately written in `plain sh`, no special bash features or
other additional tools are required.

Feel free to have a look at what `zync` does. As usual for shell scripts, start
at the bottom and work your way up.

## Usage

```sh
zync [options] {sender} {receiver}
```

### Examples

#### Pull Snapshots From A Remote Server

```sh
zync nas.local:zpool/data zbak/nas/data
```

Synchronise all snapshots from the remote `zpool/data` zfs filesystem on
`nas.local` to the local `zbak/nas/data` zfs filesystem.
  - access to `nas.local` via ssh must be possible
  - `zbak/nas` must already exist on the local system

#### Push Snapshots To A Remote Backup Server

```sh
zync zpool/data bak.example.com:zbak/nas/data
```

Synchronise all snapshots from the local `zpool/data` zfs filesystem
to the remote `zbak/nas/data` zfs filesystem on `bak.example.com`.
  - access to `bak.example.com` via ssh must be possible
  - `zbak/nas` must already exist on `bak.example.com`

#### Duplicate Snapshots Locally

```sh
zync zpool/data zbak/data
```

If you only have one machine, but enough disks to duplicate important data, you
could replicate filesystems locally.

In this case, by copying all snapshots from `zpool/data` to `zbak/data`.
  - `zbak` must exist

### Options

| Option | Description                                                          |
| ------ | -------------------------------------------------------------------- |
| `-d`   | Allow deletion of expired snapshots on receiver                      |
|        | Default: snapshots are only added to the receiver, but not removed   |
|        |                                                                      |
| `-f`   | Allow sender and receiver to have different filesystem names         |
|        | Default: sender and receiver are expected to use the same filesystem |
|        | names, to minimise the risk of naming confusion                      |
|        |                                                                      |
| `-p`   | Create all missing parent datasets on the receiver                   |
|        | Default: the receiving dataset or it's direct parent must            |
|        | exist on the receiver                                                |
|        |                                                                      |
| `-r`   | Recursively synchronise the sending dataset and all its              |
|        | sub-filesystems. (default)                                           |
|        |                                                                      |
| `-n`   | Dry run - don't actually sync anything                               |
| `-x`   | Turn on shell debugging                                              |
| `-v`   | Turn on verbose messages                                             |
|        |                                                                      |
| `-h`   | Help (this usage overview)                                           |

## Trying It Out

`zync` is fairly safe to use, with the following caveats:

- `zync` works by reading data from a `{sender}` (_source_ file system) and
  _**writing data to a `receiver`**_ (_target_ filesystem)

- when starting with `zync`, try using the `-n` (dry run) and `-v` (verbose)
  options, to get used to what `zync` does.

- also, practice with an empty `receiver`

e.g.:

```
zync -v some/zfs/data puddle/zync/test
```

## Motivation

There are several "zfs backup" systems available already, however, all of
the ones that I have encountered so far (see below) tend towards being overly
complex, require all sorts of additional packages (python, perl, mbuffer...)
and then often don't work the way I expect them to.

Although these situations are relatively easy to fix, the work required can
be daunting and is far from self-explanatory.
In my case, I am synchronising a tree of 60 filesystems, manually
_correcting_ problems for each filesystem individually is just impractical.

I'm not blaming the zfs developers, but it would have been nice if the
"problems associated with zfs send -R", and their solutions, would have
been much more straight forward and better documented.

## Why not "just use `zfs send -R`" ?

Initially I hoped that `zfs send -R {sender} | zfs recv {receiver}" would
synchronise all the snapshots on all the file systems under the {sender}.
I would set up a cron job and let zfs work out the details.

And it almost does, initially...

However, it fails miserably when the structure of your filesystems changes
(new or deleted filesystems) or if any transfers have been interrupted
(snapshot `@xyz` has been transfered for some filesystems but not for others).

Note that `zfs send -R` is only capable of sending a filesystem tree if:

1. the filesystem tree doesn't exist on the target filesystem at
   all (initial transfer)

2. or, the source and target filesystems have exactly the same
   structure and reference-snapshot (!)

These restrictions mean that `zfs send -R` is essentially only useful for
an initial backup, since any changes to the filesystem structure, or any
transfer failures (i.e. network interruptions) lead to a situation where
each filesystem needs to be handled separately.

### What Does `zync` Do Differently?

By default, `zync` synchronises an entire filesystem tree recursively.

It makes no assumptions about the snapshot names or that there be any
correlation between snapshots in different filesystems.

`zync` always applies all updates to a parent filesystem, before attempting to
perform any actions on sub-filesystems.

`zync` takes all measures necessary to keep the `{receiver}` identical to the
`{sender}`.

- filesystems which only exist on the `{sender}` are created
- _TODO_ filesystems which only exist on the `{receiver}` can be _**destroyed**_
  - _not yet implemented_
- snapshots which only exist on the `{sender}` are copied to the `{receiver}`
- snapshots which only exist on the `{receiver}` are "expired" and can be
  _**destroyed**_ on the `{receiver}`

## See also

| Tool                 | Description                                                                          |
| -------------------- | ------------------------------------------------------------------------------------ |
| `sanoid` / `syncoid` | Policy-driven snapshot management tool for ZFS filesystems                           |
|                      | https://github.com/jimsalterjrs/sanoid                                               |
|                      | perl                                                                                 |
|                      | config via INI files                                                                 |
|                      | overkill, incl. mbuffer to speed up snapshot transfers                               |
|                      | (only interesting for initial backup - incremental updates are tiny and fast anyway) |
|                      |                                                                                      |
| `zap`                | Maintain and replicate ZFS snapshots                                                 |
|                      | https://github.com/Jehops/zap                                                        |
|                      | very small code base (shell script and man page)                                     |
|                      | sh only                                                                              |
|                      |                                                                                      |
| `zfs-periodic`       | Simple way of maintaining zfs snapshots using the periodic system                    |
|                      | https://github.com/ross/zfs-periodic                                                 |
|                      | sh only?                                                                             |
|                      | periodic system?                                                                     |
|                      |                                                                                      |
| `zfs-replicate`      | ZFS Snapshot Replication Script                                                      |
|                      | https://github.com/leprechau/zfs-replicate                                           |
|                      | bash(!)                                                                              |
|                      |                                                                                      |
| `zfs-snapshot-clean` | Tool to sieve ZFS snapshots as per given spec a la 'pdumpfs-clean'                   |
|                      | sh only                                                                              |
|                      | VERY limited, not even a readme explaining the author's intentions                   |
|                      |                                                                                      |
| `zfs-snapshot-mgmt`  | Automatic ZFS snapshot management tool                                               |
|                      | defunct - predecessor to zsnap                                                       |
|                      |                                                                                      |
| `zfsnap`             | Simple sh script to make zfs rolling snaphosts with cron                             |
| `zfsnap2`            | Portable performant script to make rolling ZFS snapshots easy                        |
|                      | https://github.com/zfsnap/zfsnap                                                     |
|                      | sh                                                                                   |
|                      | creation date and time-to-live in snapshot name                                      |
|                      | version 2.0 stalled?                                                                 |
|                      |                                                                                      |
| `zfstools`           | OpenSolaris-compatible auto snapshotting for ZFS                                     |
|                      | https://github.com/bdrewery/zfstools                                                 |
|                      | ruby                                                                                 |
|                      | handles empty snapshot 'in its own special way'                                      |
|                      | uses gherkin... for 1 dummy feature :-                                               |
|                      |                                                                                      |
| `znapzend`           | https://www.znapzend.org/                                                            |
|                      | perl                                                                                 |
|                      | config via ZFS parameters                                                            |
|                      |                                                                                      |
| `zrep`               | https://github.com/bolthole/zrep                                                     |
|                      | sh only                                                                              |
|                      | has problems with recurseively structured filesystems                                |
|                      | i.e.: add/remove filesystem causes problems                                          |
|                      |                                                                                      |
| `zrepl`              | ZFS dataset replication tool                                                         |
|                      | https://zrepl.github.io                                                              |
|                      | golang                                                                               |
|                      | sh for build                                                                         |
|                      | python for documentation                                                             |
|                      | YAML configuration                                                                   |
|                      | VERY well documented                                                                 |
|                      | lots of features                                                                     |
|                      | active development (900+ commits most recent 2020-12)                                |
|                      | probably overkill, done properly?                                                    |
|                      |                                                                                      |
| `zsd`                | Destroys ZFS snapshots                                                               |
|                      | https://www.fabiankeil.de/gehacktes/zsd/                                             |
|                      | name says it all: gehacktes                                                          |
|                      | no git repo                                                                          |
|                      |                                                                                      |
| `zsm`                | ZFS Snapshot Manager                                                                 |
|                      | https://gitlab.com/thnee/zsm/                                                        |
|                      | python                                                                               |
|                      |                                                                                      |
| `zfs-snap-manager`   | ZFS Snapshot Manager                                                                 |
|                      | https://github.com/khenderick/zfs-snap-manager                                       |
|                      | python                                                                               |
|                      |                                                                                      |
| `zxfer`              | ZFS Transfer                                                                         |
|                      | https://github.com/allanjude/zxfer                                                   |
|                      | sh                                                                                   |
|                      | target file systems seem to have snapshots, but no files?!?!                         |
|                      | (i.e.: targets are missing for some reason)                                          |
|                      | -> problem seems to be that the file systems                                         |
|                      | ARE there, but unmounted, although they are mounted...                               |
|                      | strange?!?!                                                                          |

## Author

Copyright 2024, Stephen Riehm
