#!/bin/sh

# Problem:
#
#       pipe_source | while read ...; do ... ssh ... done
#
#   fails because ssh reads from STDIN, which then slurps any remaining input
#   and throws it at the remote machine, thus ending the loop
#
# Solutions:
#
#   0. best solution I found so far: redirect stdin to a "command group"
#      surrounding the while loop:
#
#       pipe_source \
#       |   while read ...
#           do
#               {               # command group
#               ...             # any commands, possibly including ssh etc
#               } < /dev/null   # command group gets its own STDIN
#           done
#
#   1. don't pipe to a while loop
#       (any embedded STDIN reader can cause the loop to terminate)
#       alternative:
#           for var in $( pipe_source )
#           do
#               # anything you like...
#           done
#
#   2. always redirect stdin to ssh etc.
#       or use ssh -n
#
#       pipe_source \
#       | while read ...
#       do
#           $( ssh < /dev/null )
#       done
#

seq 10 \
| while read blah
do
    {
    echo working on "${blah}"
    foo=$( ssh localhost seq 5 | tail -n 1 )
    echo got foo "${foo}"
    echo still working
    } < /dev/null
done
