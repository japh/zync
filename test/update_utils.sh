#!/bin/sh

#
# Unix provides several programs for comparing similar files, including:
#
#   join(1)     - Display lines which share common 'field values'.
#                 join(1) assumes that the files are sorted in lexographical order.
#
#   comm(1)     - Display the contents of two files in three columns
#                 (indented by 0, 1 or 2 tabs) representing:
#                 'left file only', 'right file only' and 'common to both files'.
#                 comm(1) assumes that the files are sorted in lexographical order.
#
#   diff(1)     - Display differences between two files.
#                 diff(1) has no sorting requirements.
#                 If two files are identical, no output is produced.
#
#   sdiff(1)    - Display differences between two files side-by-side
#                 sdiff(1) has no sorting requirements.
#                 If two files are identical, they are still displayed in full.
#
# This test shows that join(1) and comm(1) are NOT suitable for comparing
# snapshots in CHRONOLOGICAL order, as they can produce 'holes'.
# Also, diff(1) produces no output when two files are identical, this is also
# undesirable behaviour.
#
# Test Cases:
#
# To demonstrate the difference between chronological and lexographical sorting,
# we simulate a filesystem with snapshots prefixed with their 'time-frame' of
# 'hourly', 'daily', 'weekly' and 'monthly' followed by a numerical time-stamp.
# (see 'timestamp t0' in the table below)
#
# After 2 days, 2 older 'daily' snapshots will have been replaced by newer
# 'daily' snapshots, and all of the older 'hourly' snapshots have been replaced
# by newer snapshots. (see 'timestamp t1' in the table below)
#
# So we have a list of snapshots, in chronological order, as of two points in
# time, t0 and t1, two days later:
#
#   | t0 (receiver)       | change    | t1 (sender)         |
#   | ------------------- | --------- | ------------------- |
#   | @daily-20200101     | destroyed |                     |
#   | @weekly-20200101    | =>        | @weekly-20200101    |
#   | @monthly-20200101   | =>        | @monthly-20200101   |
#   | @daily-20200102     | destroyed |                     |
#   | @daily-20200103     | =>        | @daily-20200103     |
#   | @daily-20200104     | =>        | @daily-20200104     |
#   | @daily-20200105     | =>        | @daily-20200105     |
#   | @daily-20200106     | =>        | @daily-20200106     |
#   | @daily-20200107     | =>        | @daily-20200107     |
#   | @weekly-20200107    | =>        | @weekly-20200107    |
#   | @hourly-20200107-00 | destroyed |                     |
#   | @hourly-20200107-01 | destroyed |                     |
#   | @hourly-20200107-02 | destroyed |                     |
#   |                     | created   | @daily-20200108     |
#   |                     | created   | @daily-20200109     |
#   |                     | created   | @hourly-20200109-02 |
#   |                     | created   | @hourly-20200109-03 |
#   |                     | created   | @hourly-20200109-04 |
#
# Here, we can easily see that @weekly-20200107 is the most recent common
# snapshot, and that @hourly-20200109-04 the most recent new snapshot.
#
# Of course there are other naming schemes, but zync should not (silently) let
# users down in these situations.
#
# Let's see what join(1) and comm(1), diff(1) and sdiff(1) make of this data...
#

cleanup() {
    if [ -d "${test_dir}" ]
    then
        rm -rf "${test_dir}"
    fi
}

setup_test_files() {

    test_dir=$( mktemp -d )
    trap cleanup EXIT

    stats_summary_file="${test_dir}/stats_summary"

    empty_file="${test_dir}/empty_file"
    touch "${empty_file}"

    # timestamp tx: very old expired state, with NO common snapshots
    unrelated_snaps_file="${test_dir}/unrelated_snaps"
    cat << _EO_Tx_ > "${unrelated_snaps_file}"
@daily-2019-01-01-00:00
@weekly-2019-01-01-00:00
@monthly-2019-01-01-00:00
@daily-2019-01-02-00:00
_EO_Tx_

    # timestamp t0: initial state (replicated on target filesystem)
    t0_snaps_file="${test_dir}/t0_snaps"
    cat << _EO_T0_ > "${t0_snaps_file}"
@daily-2021-01-01-00:00
@weekly-2021-01-01-00:00
@monthly-2021-01-01-00:00
@daily-2021-01-02-00:00
@daily-2021-01-03-00:00
@daily-2021-01-04-00:00
@daily-2021-01-05-00:00
@daily-2021-01-06-00:00
@daily-2021-01-07-00:00
@daily-2021-01-08-00:00
@weekly-2021-01-08-00:00
@daily-2021-01-09-00:00
@daily-2021-01-10-00:00
@daily-2021-01-11-00:00
@daily-2021-01-12-00:00
@daily-2021-01-13-00:00
@daily-2021-01-14-00:00
@daily-2021-01-15-00:00
@weekly-2021-01-15-00:00
@daily-2021-01-16-00:00
@daily-2021-01-17-00:00
@daily-2021-01-18-00:00
@daily-2021-01-19-00:00
@daily-2021-01-20-00:00
@daily-2021-01-21-00:00
@daily-2021-01-22-00:00
@weekly-2021-01-22-00:00
@daily-2021-01-23-00:00
@daily-2021-01-24-00:00
@daily-2021-01-25-00:00
@daily-2021-01-26-00:00
@daily-2021-01-27-00:00
@hourly-2021-01-27-14:00
@hourly-2021-01-27-15:00
@hourly-2021-01-27-16:00
@hourly-2021-01-27-17:00
@hourly-2021-01-27-18:00
@hourly-2021-01-27-19:00
@hourly-2021-01-27-20:00
@hourly-2021-01-27-21:00
@hourly-2021-01-27-22:00
@hourly-2021-01-27-23:00
@daily-2021-01-28-00:00
@hourly-2021-01-28-00:00
@hourly-2021-01-28-01:00
@hourly-2021-01-28-02:00
@hourly-2021-01-28-03:00
@hourly-2021-01-28-04:00
@hourly-2021-01-28-05:00
@hourly-2021-01-28-06:00
@hourly-2021-01-28-07:00
@hourly-2021-01-28-08:00
@hourly-2021-01-28-09:00
_EO_T0_

    # timestamp t1: current state of source filesystem
    t1_snaps_file="${test_dir}/t1_snaps"
    cat << _EO_T1_ > "${t1_snaps_file}"
@monthly-2021-01-01-00:00
@daily-2021-01-03-00:00
@daily-2021-01-04-00:00
@daily-2021-01-05-00:00
@daily-2021-01-06-00:00
@daily-2021-01-07-00:00
@daily-2021-01-08-00:00
@weekly-2021-01-08-00:00
@daily-2021-01-09-00:00
@daily-2021-01-10-00:00
@daily-2021-01-11-00:00
@daily-2021-01-12-00:00
@daily-2021-01-13-00:00
@daily-2021-01-14-00:00
@daily-2021-01-15-00:00
@weekly-2021-01-15-00:00
@daily-2021-01-16-00:00
@daily-2021-01-17-00:00
@daily-2021-01-18-00:00
@daily-2021-01-19-00:00
@daily-2021-01-20-00:00
@daily-2021-01-21-00:00
@daily-2021-01-22-00:00
@weekly-2021-01-22-00:00
@daily-2021-01-23-00:00
@daily-2021-01-24-00:00
@daily-2021-01-25-00:00
@daily-2021-01-26-00:00
@daily-2021-01-27-00:00
@daily-2021-01-28-00:00
@daily-2021-01-29-00:00
@weekly-2021-01-29-00:00
@daily-2021-01-30-00:00
@daily-2021-01-31-00:00
@hourly-2021-01-31-00:00
@hourly-2021-01-31-01:00
@hourly-2021-01-31-02:00
@hourly-2021-01-31-03:00
@hourly-2021-01-31-04:00
@hourly-2021-01-31-05:00
@hourly-2021-01-31-06:00
@hourly-2021-01-31-07:00
@hourly-2021-01-31-08:00
@hourly-2021-01-31-09:00
_EO_T1_

    expected_common_snap_file="${test_dir}/common_snap"
    echo "@daily-2021-01-28-00:00" > "${expected_common_snap_file}"

    expected_latest_snap_file="${test_dir}/latest_snap"
    echo "@hourly-2021-01-31-09:00" > "${expected_latest_snap_file}"

    expected_expired_snaps_file="${test_dir}/expired_snaps"
    cat <<- _EO_EXPIRED_SNAPS_ > "${expected_expired_snaps_file}"
@daily-2021-01-01-00:00
@weekly-2021-01-01-00:00
@daily-2021-01-02-00:00
@hourly-2021-01-27-14:00
@hourly-2021-01-27-15:00
@hourly-2021-01-27-16:00
@hourly-2021-01-27-17:00
@hourly-2021-01-27-18:00
@hourly-2021-01-27-19:00
@hourly-2021-01-27-20:00
@hourly-2021-01-27-21:00
@hourly-2021-01-27-22:00
@hourly-2021-01-27-23:00
@hourly-2021-01-28-00:00
@hourly-2021-01-28-01:00
@hourly-2021-01-28-02:00
@hourly-2021-01-28-03:00
@hourly-2021-01-28-04:00
@hourly-2021-01-28-05:00
@hourly-2021-01-28-06:00
@hourly-2021-01-28-07:00
@hourly-2021-01-28-08:00
@hourly-2021-01-28-09:00
_EO_EXPIRED_SNAPS_

    # common file to be used for collecting results from the various programs
    results_file="${test_dir}/got"
}

reset_stats() {
    tests_run=0
    tests_passed=0
    tests_failed=0
}

print_stats() {
    cmd="${1}"
    {
        printf "${cmd}(1) "
        if [ "${tests_failed}" -ne 0 ]
        then
            printf "failed %d of %d tests\n" "${tests_failed}" "${tests_run}"
        else
            printf "PASSED ALL TESTS\n"
        fi
    } | tee -a "${stats_summary_file}"
}

print_stats_summary() {
    echo ""
    echo "#--------------------"
    echo "# TEST SUMMARY"
    echo "#"
    sed 's:^:    :' "${stats_summary_file}"
}

sample_output() {
    name="${1}"
    cmd="${2}"
    t0_file="${3}"
    t1_file="${4}"

    echo ""
    echo "#--------------------"
    echo "# Sample ${cmd} Output"
    echo "#"
    echo "# '$name'"
    echo "#"
    echo "# -- start ${cmd} output --"
    "${cmd}_output" "${t0_file}" "${t1_file}"
    echo "# -- end ${cmd} output --"
    echo "#--------------------"
}

check_expected() {
    name="${1}"
    method="${2}"
    cmd="${3}"
    t0_file="${4}"
    t1_file="${5}"
    expected_file="${6}"

    # run the command to be tested and capture any results
    "${cmd}_${method}" "${t0_file}" "${t1_file}" > "${results_file}"

    tests_run=$(( $tests_run + 1 ))

    # check if we got the expected results
    if cmp -s "${results_file}" "${expected_file}"
    then
        tests_passed=$(( $tests_passed + 1 ))
        printf "    ok - %s\n" "${cmd}(1): ${name}"
    else
        tests_failed=$(( $tests_failed + 1 ))
        printf "not ok - %s\n" "${cmd}(1): ${name}"
        printf $'\t'"%-31s %-30s\n" "Got" "Expected"
        printf $'\t'"%-31s %-20s\n" "===" "========"
        diff -y -W 60 "${results_file}" "${expected_file}" | sed 's:^:'$'\t'':'
    fi
}

run_tests() {
    cmd="${1}"

    echo ""
    echo "#--------------------"
    echo "# ${cmd}(1) tests"
    echo "#"

    reset_stats

    sample_output "initial transfer (no old snapshots)"                 "${cmd}" "${empty_file}"           "${t1_snaps_file}" 
    sample_output "re-start (old snapshots don't match new snapshots)"  "${cmd}" "${unrelated_snaps_file}" "${t1_snaps_file}" 
    sample_output "update (overlapping mix of new/old snapshots)"       "${cmd}" "${t0_snaps_file}"        "${t1_snaps_file}" 
    sample_output "no-update (all snapshots identical)"                 "${cmd}" "${t1_snaps_file}"        "${t1_snaps_file}" 

    check_expected "most recent common snapshot on empty target"            "common" "${cmd}" "${empty_file}"           "${t1_snaps_file}" "${empty_file}"
    check_expected "most recent common snapshot on unrelated target"        "common" "${cmd}" "${unrelated_snaps_file}" "${t1_snaps_file}" "${empty_file}"
    check_expected "most recent common snapshot on overlapping filesystems" "common" "${cmd}" "${t0_snaps_file}"        "${t1_snaps_file}" "${expected_common_snap_file}"
    check_expected "most recent common snapshot on identical filesystems"   "common" "${cmd}" "${t1_snaps_file}"        "${t1_snaps_file}" "${expected_latest_snap_file}"

    check_expected "expired snapshots between empty target"             "expired" "${cmd}" "${empty_file}"           "${t1_snaps_file}" "${empty_file}"
    check_expected "expired snapshots between unrelated target"         "expired" "${cmd}" "${unrelated_snaps_file}" "${t1_snaps_file}" "${unrelated_snaps_file}"
    check_expected "expired snapshots between overlapping filesystems"  "expired" "${cmd}" "${t0_snaps_file}"        "${t1_snaps_file}" "${expected_expired_snaps_file}"
    check_expected "expired snapshots between identical filesystems"    "expired" "${cmd}" "${t1_snaps_file}"        "${t1_snaps_file}" "${empty_file}"

    print_stats "${cmd}"
}

#
# Test Methods:
#
#   <cmd>_<method> <t0 file> <t1 file>
#

#
# join(1)
#
#   breaks on chronological data because it expects data to be in lexographical order
#
join_output() {
    join "${1}" "${2}"
}
join_common() {
    join "${1}" "${2}" | tail -n1
}
join_expired() {
    join -v1 "${1}" "${2}"
}

#
# comm(1)
#
#   breaks on chronological data because it expects data to be in lexographical order
#
comm_output() {
    comm -12 "${1}" "${2}"
}
comm_common() {
    comm -12 "${1}" "${2}" | tail -n1
}
comm_expired() {
    comm -23 "${1}" "${2}"
}

#
# diff(1)
#
#   breaks when both filesystems are identical
#
diff_output() {
    diff -y "${1}" "${2}"
}
diff_common() {
    diff -y "${1}" "${2}" | awk '/[<|]/ { print $1 }' | tail -n 1
}
diff_expired() {
    diff "${1}" "${2}" | awk '/^</ { print $2 }'
}

#
# sdiff(1)
#
#   works in all cases
#
sdiff_output() {
    sdiff "${1}" "${2}"
}
sdiff_common() {
    sdiff "${1}" "${2}" | awk '/^[^<>|]*$/ { print $1 }' | tail -n 1
}
sdiff_expired() {
    sdiff "${1}" "${2}" | awk '/[<|]/ { print $1 }'
}


#
# Main
#

setup_test_files

run_tests "join"
run_tests "comm"
run_tests "diff"
run_tests "sdiff"

print_stats_summary

exit
